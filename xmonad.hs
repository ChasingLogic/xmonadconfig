import XMonad
import XMonad.Util.EZConfig
import XMonad.Util.Run

main = do
    dzenLeftBar <- spawnPipe myXmonadBar
    dzenRightBar <- spawnPipe myStatusBar
    xmonad $ defaultConfig
     { terminal           = myTerminal
     , modMask            = myModKey
     , borderWidth        = myBorderWidth 
     , workspaces          = myWorkspaces
     --, normalBorderColor  = myNormalBorderColor
     --, focusedBorderColor = myFocusedBorderColor

     -- key bindings
     --, keys               = myKeys
     --, mouseBindings      = myMouseBindings

     -- hooks, layouts
     --, layoutHook      = myLayout
     --, manageHook      = myManageHook
     --, handleEventHook = myEventHook
     --, logHook         = myLogHook
     --, startupHook     = myStartupHook
     } 
     --'additionalKeys'
     --[ (("M-b"), spawn "firefox")
     --, (("M-<Return>"), spawn myTerminal)
     --]

myTerminal       = "termite"
myModKey         = mod1Mask
myBorderWidth    = 3
myWorkspaces     = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
myXmonadBar = "dzen2 -x '1440' -y '0' -h '24' -w '640' -ta 'l' -fg '#FFFFFFF' -bg '#1B1D1E'"
myStatusBar = "conky -c /home/mat/.xmonad/conky_dzen | dzen2 -x '2080' -w '1040' -h '24' -ta 'r' -bg '#1B1D1E' -fg '#FFFFFF' -y '0'"
